package com.video.game;

import java.io.Serializable;

public class Performer implements Serializable {
	private static final long serialVersionUID = 1L;
	protected int unionId;
	enum PerformerType{
		Dancer, Vocalist, Performer
	}
	
	private PerformerType performerType;
	
	public Performer() {
		// TODO Auto-generated constructor stub
	}
	
	public Performer(int unionId, PerformerType performerType) {
		this.unionId = unionId;
		this.performerType = performerType;
	}

	public int getUnionId() {
		return unionId;
	}

	public void setUnionId(int unionId) {
		this.unionId = unionId;
	}
	
	public PerformerType getPerformerType() {
		return performerType;
	}

	public void setPerformerType(PerformerType performerType) {
		this.performerType = performerType;
	}

	String perform() {
		return unionId+" - "+ PerformerType.Performer;
	}
}
