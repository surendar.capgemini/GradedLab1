package com.video.game;

public class Vocalist extends Performer {
	private static final long serialVersionUID = 1L;
	private char key;
	private int volume;

	public Vocalist(int unionId, char key, int volume) {
		super();
		super.setUnionId(unionId);
		this.key = key;
		this.volume = volume;
		if ( volume < 1 || volume > 10) {
			throw new IllegalArgumentException();
		}
	}
	
	@Override
	String perform() {
		return "I sing in the key of – " + key + " –  at the volume " + volume + " - " + getUnionId();
	}
}
