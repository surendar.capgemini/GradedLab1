package com.video.game;

public class Dancer extends Performer {
	private static final long serialVersionUID = 1L;
	private String danceStyle;

	public Dancer(int unionId, String danceStyle) {
		super.setUnionId(unionId);
		this.danceStyle = danceStyle;
	}

	@Override
	String perform() {
		return danceStyle + " - " + getUnionId() + " - " + PerformerType.Dancer;
	}

}
