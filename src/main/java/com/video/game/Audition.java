package com.video.game;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.video.game.Performer.PerformerType;

public class Audition {

	final static Logger logger = Logger.getLogger(Audition.class);
	private static Set<Performer> performers;
	
	static Set<Performer> addPerformer() {
		logger.info("Adding Performers");
		performers = new HashSet<Performer>();
		performers.add(new Performer(243, PerformerType.Performer));
		performers.add(new Performer(332, PerformerType.Performer));
		performers.add(new Performer(432, PerformerType.Performer));
		performers.add(new Performer(567, PerformerType.Performer));
		performers.add(new Dancer(678, "tap"));
		performers.add(new Dancer(789, "salsa"));
		performers.add(new Vocalist(900, 'G', 3));
		return performers;
	}

	public static void main(String[] args) {
		logger.info("Generating Audition");
		System.out.println("Generated Audition");
		System.out.println("=======================================================");
		for (Performer performer : addPerformer()) {
			System.out.println(performer.perform());
		}
	}
}
