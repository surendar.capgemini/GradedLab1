package com.video.game;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DancerTest {
	
	private Dancer dancer;
	
	@Before
	public void setUp() {
		dancer = new Dancer(772, "tap");
	}
	
	@Test
	public void testPerform() {
		assertEquals("tap - 772 - Dancer", dancer.perform());
	}

}
