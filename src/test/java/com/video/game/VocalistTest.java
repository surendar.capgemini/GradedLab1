package com.video.game;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VocalistTest {
	
	private Vocalist vocalist;
	
	@Before
	public void setUp() {
		vocalist = new Vocalist(1234, 'G', 3);
	}
	
	@Test
	public void testPerform() {
		assertEquals("I sing in the key of – G –  at the volume 3 - 1234", vocalist.perform());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testIllegalArgumentExceptionForBelowVolumeRange() {
	    vocalist = new Vocalist(1122, 'H', 0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testIllegalArgumentExceptionForAboveVolumeRange() {
	    vocalist = new Vocalist(1122, 'H', 17);
	}
}
